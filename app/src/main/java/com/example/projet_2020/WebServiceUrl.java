package com.example.projet_2020;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WebServiceUrl
{

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";
    private static final String PATH_2 = "items";
    private static final String PATH_3 = "thumbnail";
    private static final String URL_PARAM1_1 = "ids";
    private static final String URL_PARAM1_2 = "categories";
    private static final String URL_PARAM1_3 = "catalog";
    JSONArray jsonArray ;
    List<String> listElement ;
    String[] stringArrayElement = null;

    //fonctionnalité qui cree une url avec 1 seul param => ex:https://demo-lia.univ-avignon.fr/cerimuseum/categorie
    public static URL build(String chaine) throws MalformedURLException
    {
        if(chaine == URL_PARAM1_1 || chaine == URL_PARAM1_2 || chaine == URL_PARAM1_3 )
        {
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("https")
                    .appendPath(PATH_1)
                    .authority(HOST)
                    .appendPath(chaine);
            URL url = new URL(builder.build().toString());
            Log.i("Creation d'url","url : "+url);
            return url;
        }
        else
        {
            return null;
        }

    }

    //fonctionnalité qui cree une url avec 2(items,ace) params => ex:https://demo-lia.univ-avignon.fr/cerimuseum/items/ace
    public static URL buildCatalog(String param) throws MalformedURLException
    {

            Uri.Builder builder = new Uri.Builder();
            builder.scheme("https")
                    .appendPath(PATH_1)
                    .appendPath(PATH_2)
                    .authority(HOST)
                    .appendPath(param);
            URL url = new URL(builder.build().toString());
            Log.i("Creation d'url catalog ","url catalog : "+url);
            return url;

    }
    public static URL buildImage(String param) throws MalformedURLException
    {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(param)
                .appendPath(PATH_3)
                .authority(HOST);
        URL url = new URL(builder.build().toString());
        Log.i("Creation d'url image ","url image : "+url);
        return url;

    }

    //Connection url
    public List<String> downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        URL url = new URL(myurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try
        {

            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);

            // debut requete serveur
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("response", "The response is: " + response);
            is = conn.getInputStream();

            // Convertion inputstream en string
            String contentAsString = readIt(is);
            Log.d("retour readIt","retour :"+contentAsString);
            return listElement;
        }
        finally
        {
            if (is != null) {
                is.close();
                conn.disconnect();

            }
        }
    }

    //fonction qui lit le inputstream
    public String readIt(InputStream stream) throws IOException, UnsupportedEncodingException {
        Log.d("function readIt","entre readIt");
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = reader.readLine()) != null)
        {
            builder.append(line + "\n");
            Log.d("String line","line:"+line);
        }
        try
        {
            jsonArray = new JSONArray(builder.toString());
            Log.d("array length", "json array length: "+jsonArray.length());

            listElement = new ArrayList<String>();
            for (int i=0; i<jsonArray.length(); i++)
            {
                listElement.add( jsonArray.getString(i) );
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        return builder.toString();
    }

    //fonction qui recup les elements d'un json array en array normal pour utiliser en listview par exemple
    public static String[] getStringArray(JSONArray jsonArray) {
        String[] stringArray = null;
        if (jsonArray != null) {
            int length = jsonArray.length();
            stringArray = new String[length];
            for (int i = 0; i < length; i++)
            {
                stringArray[i] = jsonArray.optString(i);
                Log.d("array element", "json array element:"+ stringArray[i]);
            }
        }
        return stringArray;
    }
}
