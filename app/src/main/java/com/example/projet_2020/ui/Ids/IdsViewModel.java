package com.example.projet_2020.ui.Ids;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class IdsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public IdsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Voici la liste des id de produit");
    }

    public LiveData<String> getText() {
        return mText;
    }
}