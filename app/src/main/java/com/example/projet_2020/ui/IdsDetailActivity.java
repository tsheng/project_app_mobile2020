package com.example.projet_2020.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projet_2020.R;
import com.example.projet_2020.WebServiceUrl;
import com.example.projet_2020.ui.catalogue.FetchDataCatalog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.BreakIterator;


public class IdsDetailActivity extends Fragment {

    public static TextView id;
    public static TextView annee;
    public static TextView detailsTech;
    public static TextView description;
    public static TextView nomProduit;
    public static TextView marque;
    public static TextView anneeProduit;
    public static TextView categorie;
    public static ImageView imageProd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_ids_detail_activity, container, false);
        Bundle bundle = getArguments();
        Log.d("idBundle","bundle Id:"+bundle);
        String monId = bundle.getString("idbundle");
//        String prodNom = bundle.getString("nom");
//        String prodAnnee = bundle.getString("annee");
//        String prodDesc = bundle.getString("desc");
//        String prodMarque = bundle.getString("marque");

//        FetchDataCatalog fetchDataCatalog = new FetchDataCatalog();
//        fetchDataCatalog.execute(monId);

        //recherche  les elements du xml
        id = root.findViewById(R.id.idProduit);
        annee = root.findViewById(R.id.anneeProduit);
        detailsTech = root.findViewById(R.id.detailsTech);
        description = root.findViewById(R.id.description);
        nomProduit = root.findViewById(R.id.nomProduit);
        marque = root.findViewById(R.id.marque);
        anneeProduit = root.findViewById(R.id.anneeProduit);
        categorie = root.findViewById(R.id.catProduit);
        imageProd = root.findViewById(R.id.imageProduit);


        //ecrit dans le champs idproduit
        id.setText(monId);
//        nomProduit.setText(prodNom);
//        description.setText(prodDesc);
//        marque.setText(prodMarque);
//        annee.setText(prodAnnee);
        return root;
    }
}
