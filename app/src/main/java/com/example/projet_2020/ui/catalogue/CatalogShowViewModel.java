package com.example.projet_2020.ui.catalogue;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CatalogShowViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CatalogShowViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("La liste des catalogue !");
    }

    public LiveData<String> getText() {
        return mText;
    }
}