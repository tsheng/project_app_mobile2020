package com.example.projet_2020.ui.catalogue;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.projet_2020.WebServiceUrl;
import com.example.projet_2020.ui.IdsDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FetchDataCatalog extends AsyncTask<String, Void, Void> {
    IdsDetailActivity idsDetailActivity = new IdsDetailActivity();
    String data ;

    String nom;
    String desc;
    String brand;
    String year;
    String detailsTechnique;
    String categorieProd;

    @Override
    protected Void doInBackground(String... strings) {
        URL url = null;

        Log.d("param async","param async"+strings[0]);
        try
        {
            //instanciation nouvelle url
            WebServiceUrl catalogDetail = new WebServiceUrl();

            Log.d("string0","id :"+strings[0]);
            //cree l'url avec le param[0]
            url = catalogDetail.buildCatalog(strings[0]);

            Log.d("catalog","catalog url:"+url.toString());
            //connection
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader =  new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while(line != null)
            {
                line = bufferedReader.readLine();
                //ajout line dans data
                data = data + line;;
            }
            //supprime null en debut de retour de data car retour = "null{etc...}"
            data = data.replace("null","");
            Log.d("data","data :"+data);

            //cree jsonObject avec retour data
            JSONObject jo = new JSONObject(data);
            Log.d("jsonObj","jsonobject"+jo);


            //récup les detail technique du produit
            JSONArray detailTechnique = jo.getJSONArray("technicalDetails");
            Log.d("detail techn","detail tech :"+detailTechnique.toString());
            detailsTechnique = getStringArray(detailTechnique);
            Log.d("retour detail technique","valeur :"+detailsTechnique);

            //recup nom du produit
            nom = (String) jo.get("name");
            Log.d("nom","nom prod :"+nom);

            //recup description produit

            desc = (String) jo.get("description");
            Log.d("desc","desc prod :"+desc);

            //recup marque produit
            brand = (String) jo.get("brand");
            Log.d("brand","brand prod :"+brand);

            //recup année produit
            year = "" + jo.get("year") ;
            Log.d("year","year prod :"+year);



            JSONArray cat = jo.getJSONArray("categories");
            Log.d("cate","cate:"+cat.toString());
            categorieProd = getStringArray(cat);
            Log.d("retour cate","valeur :"+categorieProd);
            //categorie = (String) jo.get("categorie");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

//        Bundle bundle = new Bundle();
//        bundle.putString("annee",year);
//        bundle.putString("desc",desc);
//        bundle.putString("nom",nom);
//        bundle.putString("brand",brand);
//        idsDetailActivity.setArguments(bundle);

        //set les éléments du fragment IdsDetaileActivity
        IdsDetailActivity.detailsTech.setTextSize(12);
        IdsDetailActivity.annee.setText(year);

        IdsDetailActivity.description.setTextSize(12);
        IdsDetailActivity.description.setText(desc);
        IdsDetailActivity.nomProduit.setTextSize(12);
        IdsDetailActivity.nomProduit.setText(nom);
        IdsDetailActivity.marque.setTextSize(12);
        IdsDetailActivity.marque.setText(brand);
        IdsDetailActivity.detailsTech.setTextSize(12);
        IdsDetailActivity.detailsTech.setText(detailsTechnique);
        IdsDetailActivity.categorie.setTextSize(12);
        IdsDetailActivity.categorie.setText(categorieProd);
    }

    public static String getStringArray(JSONArray jsonArray) {
        String[] stringArray = null;
        String retour = "";
        if (jsonArray != null) {
            int length = jsonArray.length();
            stringArray = new String[length];
            for (int i = 0; i < length; i++)
            {
                stringArray[i] = jsonArray.optString(i);
                Log.d("array element", "json array element:"+ stringArray[i]);
                if(i==length-1)
                {
                    retour = retour + stringArray[i];
                    Log.d("sans virgule","sans virgule");
                }
                else
                {
                    retour = retour + stringArray[i]+",";
                    Log.d("virgule","virgule");
                }
            }
        }
        return retour;
    }
}
