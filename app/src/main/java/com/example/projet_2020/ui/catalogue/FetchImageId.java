package com.example.projet_2020.ui.catalogue;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.projet_2020.WebServiceUrl;
import com.example.projet_2020.ui.IdsDetailActivity;

import java.io.InputStream;
import java.net.URL;

public class FetchImageId extends AsyncTask<String, Void, Bitmap> {

    @Override
    protected Bitmap doInBackground(String... strings) {

        URL urlImg = null;
        Bitmap mIcon11 = null;
        try {
            WebServiceUrl image = new WebServiceUrl();
            URL urldisplay = image.buildImage(strings[0]);
            Log.d("urldisplay to string","url img :"+urldisplay.toString());
            InputStream in = new java.net.URL(urldisplay.toString()).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.d("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        IdsDetailActivity.imageProd.setImageBitmap(result);
    }
}
