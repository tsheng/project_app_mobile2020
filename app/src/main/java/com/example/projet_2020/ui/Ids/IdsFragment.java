package com.example.projet_2020.ui.Ids;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.projet_2020.R;
import com.example.projet_2020.WebServiceUrl;
import com.example.projet_2020.ui.IdsDetailActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class IdsFragment extends Fragment {

    private IdsViewModel idsViewModel;
    public URL url;
    List<String> IdsElement ;
    ListView listView ;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        idsViewModel = ViewModelProviders.of(this).get(IdsViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_ids, container, false);

        idsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                try
                {
                    WebServiceUrl webServiceUrl = new WebServiceUrl();
                    url = webServiceUrl.build("ids");
                    IdsElement = webServiceUrl.downloadUrl(url.toString());
                    Log.d("String list","List :"+IdsElement);
                    final ListView listView = root.findViewById(R.id.ListviewIds);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,IdsElement);
                    listView.setAdapter(adapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @SuppressLint("ResourceType")
                        @Override

                        public void onItemClick(AdapterView parent, View v, int position, long id) {
                            String monIdProduit = parent.getItemAtPosition(position).toString();
                            Toast.makeText(getActivity(), "You selected:" + monIdProduit,Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        return root;
    }
}
