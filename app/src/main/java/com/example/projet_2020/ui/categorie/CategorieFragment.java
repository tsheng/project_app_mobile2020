package com.example.projet_2020.ui.categorie;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.projet_2020.R;
import com.example.projet_2020.WebServiceUrl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class CategorieFragment extends Fragment {

    public CategorieViewModel homeViewModel;
    public URL url;
    List<String> categorieElement ;

    SimpleCursorAdapter cursorAdapter;
    ListView listView ;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(CategorieViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_categorie, container, false);

        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                WebServiceUrl webServiceUrl = new WebServiceUrl();
                try
                {
                    url = webServiceUrl.build("categories");
                    categorieElement = webServiceUrl.downloadUrl(url.toString());

                    Log.d("String list categorie","List categorie:"+categorieElement);
                    final ListView listView = root.findViewById(R.id.ListviewCategorie);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,categorieElement);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override

                        public void onItemClick(AdapterView parent, View v, int position, long id) {
                            String maCategorie = parent.getItemAtPosition(position).toString();
                            Toast.makeText(getActivity(), "You selected:" + maCategorie,Toast.LENGTH_LONG).show();
//                            Intent intent = new Intent(getActivity(), produitActivity.class);
////                            //intent on envoi le vin
////                            intent.putExtra("id_produit","");
////                            //lancmenet intent
////                            startActivityForResult(intent,1);
                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        return root;
    }

}
