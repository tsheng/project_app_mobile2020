package com.example.projet_2020.ui.categorie;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CategorieViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CategorieViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Les catégories de la collection");
    }

    public LiveData<String> getText() {
        return mText;
    }
}