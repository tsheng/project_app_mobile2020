package com.example.projet_2020.ui.catalogue;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projet_2020.R;
import com.example.projet_2020.WebServiceUrl;
import com.example.projet_2020.ui.IdsDetailActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class CatalogShowFragment extends Fragment {

    private CatalogShowViewModel listeShowViewModel;
    String data ;

    String nom;
    String desc;
    String brand;
    String year;
    RecyclerView recyclerView;
    private static final int NO_STATUS_CODE = 0;
    public URL url;
    List<String> catalogElement ;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        listeShowViewModel = ViewModelProviders.of(this).get(CatalogShowViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_catalog, container, false);
        listeShowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Button search = root.findViewById(R.id.buttonSearchCatalog);
                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //recup text du xml
                        EditText text = root.findViewById(R.id.IdCatalogResearch);
                        //converti en strind et en minuscule le string
                        String monIdProduit = text.getText().toString().toLowerCase();

                        URL url = null;

                        try
                        {
                            //cree asynctask avec argument entré dans le EditText
                           FetchDataCatalog fetchDataCatalog = new FetchDataCatalog();

                           Log.d("id","id :"+monIdProduit);
                           fetchDataCatalog.execute(monIdProduit);

                           FetchImageId fetchImageId = new FetchImageId();
                           fetchImageId.execute(monIdProduit);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Bundle bundle = new Bundle();
                        //ajout id aux données à envoyer
                        bundle.putString("idbundle",monIdProduit);
//                        bundle.putString("nom",nom);
//                        bundle.putString("desc",desc);
//                        bundle.putString("annee",year);
//                        bundle.putString("marque",brand);

                        //Cree nouveau frag manager
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        Log.d("fragmentManag","fragment :"+fragmentManager.toString());
                        //debut
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        //cree un nouveau fragment qui contiendra les détails du produit entré
                        IdsDetailActivity idsDetailActivity = new IdsDetailActivity();
                        //envoi les variable à l'autre fragment
                        idsDetailActivity.setArguments(bundle);
                        //remplace le framgent actuel par le fragment idsDetailActivity
                        fragmentTransaction.replace(R.id.details,idsDetailActivity);
                        //lanceement
                        fragmentTransaction.commit();
                    }
                });
            }
        });

        return root;
    }

}
